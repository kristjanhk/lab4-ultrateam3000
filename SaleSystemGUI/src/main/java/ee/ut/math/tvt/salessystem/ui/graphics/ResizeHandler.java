package ee.ut.math.tvt.salessystem.ui.graphics;

import javafx.event.EventHandler;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TabPane;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class ResizeHandler implements EventHandler<MouseEvent> {
    private final Stage stage;
    private Cursor cursor = Cursor.DEFAULT;
    private double startX = 0;
    private double startY = 0;

    public ResizeHandler(Stage stage) {
        this.stage = stage;
    }

    public Cursor getCursor() {
        return this.cursor;
    }

    public void addResizeListener(Scene scene) {
        scene.addEventHandler(MouseEvent.MOUSE_MOVED, this);
        scene.addEventHandler(MouseEvent.MOUSE_PRESSED, this);
        scene.addEventHandler(MouseEvent.MOUSE_DRAGGED, this);
        scene.getRoot().getChildrenUnmodifiable().forEach(child -> addDeepListener(child, this));
    }

    private void addDeepListener(Node node, EventHandler<MouseEvent> listener) {
        node.addEventHandler(MouseEvent.MOUSE_MOVED, listener);
        node.addEventHandler(MouseEvent.MOUSE_PRESSED, listener);
        node.addEventHandler(MouseEvent.MOUSE_DRAGGED, listener);
        if (node instanceof TabPane) {
            ((TabPane) node).getTabs().forEach(tab -> addDeepListener(tab.getContent(), listener));
        } else if (node instanceof SplitPane) {
            ((SplitPane) node).getItems().forEach(child -> addDeepListener(child, listener));
        } else if (node instanceof TitledPane) {
            addDeepListener(((TitledPane) node).getContent(), listener);
        } else if (node instanceof Parent) {
            ((Parent) node).getChildrenUnmodifiable().forEach(child -> addDeepListener(child, listener));
        }
    }

    @Override
    public void handle(MouseEvent e) {
        if (MouseEvent.MOUSE_MOVED.equals(e.getEventType())) {
            changeCursor(e);
        } else if (MouseEvent.MOUSE_PRESSED.equals(e.getEventType())) {
            saveDragStartPoint(e);
        } else if (MouseEvent.MOUSE_DRAGGED.equals(e.getEventType())) {
            changeSize(e);
        }
    }

    private void changeCursor(MouseEvent e) {
        cursor = getCursor(stage.getScene(), e);
        stage.getScene().setCursor(cursor);
    }

    private void saveDragStartPoint(MouseEvent e) {
        startX = stage.getWidth() - e.getSceneX();
        startY = stage.getHeight() - e.getSceneY();
    }

    private void changeSize(MouseEvent e) {
        if (Cursor.DEFAULT.equals(cursor) || stage.isMaximized()) {
            return;
        }
        if (!Cursor.W_RESIZE.equals(cursor) && !Cursor.E_RESIZE.equals(cursor)) {
            changeHeight(e);
        }
        if (!Cursor.N_RESIZE.equals(cursor) && !Cursor.S_RESIZE.equals(cursor)) {
            changeWidth(e);
        }
    }

    private void changeHeight(MouseEvent e) {
        if (Cursor.NW_RESIZE.equals(cursor) || Cursor.N_RESIZE.equals(cursor) || Cursor.NE_RESIZE.equals(cursor)) {
            if (stage.getHeight() > stage.getMinHeight() || e.getSceneY() < 0) {
                if (stage.getY() - e.getScreenY() + stage.getHeight() > stage.getMinHeight()) {
                    stage.setHeight(stage.getY() - e.getScreenY() + stage.getHeight());
                    stage.setY(e.getScreenY());
                }
            }
        } else {
            if (stage.getHeight() > stage.getMinHeight() || e.getSceneY() + startY - stage.getHeight() > 0) {
                if (e.getSceneY() + startY > stage.getMinHeight()) {
                    stage.setHeight(e.getSceneY() + startY);
                }
            }
        }
    }

    private void changeWidth(MouseEvent e) {
        if (Cursor.NW_RESIZE.equals(cursor) || Cursor.W_RESIZE.equals(cursor) || Cursor.SW_RESIZE.equals(cursor)) {
            if (stage.getWidth() > stage.getMinWidth() || e.getSceneX() < 0) {
                if (stage.getX() - e.getScreenX() + stage.getWidth() > stage.getMinWidth()) {
                    stage.setWidth(stage.getX() - e.getScreenX() + stage.getWidth());
                    stage.setX(e.getScreenX());
                }
            }
        } else {
            if (this.stage.getWidth() > stage.getMinWidth() || e.getSceneX() + startX - this.stage.getWidth() > 0) {
                if (e.getSceneX() + startX > stage.getMinWidth()) {
                    this.stage.setWidth(e.getSceneX() + startX);
                }
            }
        }
    }

    private Cursor getCursor(Scene scene, MouseEvent e) {
        int border = 8;
        if (this.stage.isMaximized()) {
            return Cursor.DEFAULT;
        } else if (e.getSceneX() < border && e.getSceneY() < border) {
            return Cursor.NW_RESIZE;
        } else if (e.getSceneX() < border && e.getSceneY() > scene.getHeight() - border) {
            return Cursor.SW_RESIZE;
        } else if (e.getSceneX() > scene.getWidth() - border && e.getSceneY() < border) {
            return Cursor.NE_RESIZE;
        } else if (e.getSceneX() > scene.getWidth() - border && e.getSceneY() > scene.getHeight() - border) {
            return Cursor.SE_RESIZE;
        } else if (e.getSceneX() < border) {
            return Cursor.W_RESIZE;
        } else if (e.getSceneX() > scene.getWidth() - border) {
            return Cursor.E_RESIZE;
        } else if (e.getSceneY() < border) {
            return Cursor.N_RESIZE;
        } else if (e.getSceneY() > scene.getHeight() - border) {
            return Cursor.S_RESIZE;
        } else {
            return Cursor.DEFAULT;
        }
    }
}