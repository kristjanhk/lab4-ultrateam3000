package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.ui.common.Fxml;
import ee.ut.math.tvt.salessystem.ui.controllers.MainController;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Graphical user interface of the sales system.
 */
public class SalesSystemUI extends Application {
    private static final Logger log = LogManager.getLogger(SalesSystemUI.class);
    private final int[] defaultSizes = new int[]{600, 500};
    private final SalesSystemDAO dao;
    private final ShoppingCart shoppingCart;

    /**
     * Creates initial data access object and cart.
     */
    public SalesSystemUI() {
        dao = new HibernateSalesSystemDAO();
        shoppingCart = new ShoppingCart(dao);
    }

    /**
     * Starts the application:
     *
     * Creates a new instance of MainController,
     * creates scene from controllers node,
     * sets stylesheet,
     * MainController initializes its own content,
     * sets stage info,
     * starts the application.
     *
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        log.info("javafx version: " + System.getProperty("javafx.runtime.version"));
        MainController controller = MainController.newInstance(Fxml.TABMANAGER);
        Scene scene = new Scene(controller.getParent(), defaultSizes[0], defaultSizes[1]);
        scene.getStylesheets().add(getClass().getResource("theme.css").toExternalForm());
        controller.initializeContent(scene, primaryStage, dao, shoppingCart);
        primaryStage.setTitle("Sales system");
        primaryStage.setMinWidth(defaultSizes[0]);
        primaryStage.setMinHeight(defaultSizes[1]);
        primaryStage.setScene(scene);
        primaryStage.setOnCloseRequest(event -> dao.close());
        primaryStage.show();
        log.info("Salesystem GUI started");
    }
}