package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.data.HistoryItem;
import ee.ut.math.tvt.salessystem.data.SoldItem;
import ee.ut.math.tvt.salessystem.ui.common.EnableDataCopy;
import ee.ut.math.tvt.salessystem.ui.common.Validation;
import ee.ut.math.tvt.salessystem.ui.graphics.tableview.HistoryCartTableRow;
import ee.ut.math.tvt.salessystem.ui.graphics.tableview.HistoryCartToggleCell;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.table.TableRowExpanderColumn;

import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryController extends AbstractController {
    private static final Logger log = LogManager.getLogger(HistoryController.class);
    @FXML private DatePicker startDatePicker;
    @FXML private DatePicker endDatePicker;
    @FXML private Text dailyIncomeField;
    @FXML private Button showBetweenDates;
    @FXML protected Button showLast10;
    @FXML protected Button showAll;
    @FXML private TableView<HistoryItem> historyTableView;
    @FXML private TableView<SoldItem> cartTableView;
    @FXML private TextField productFilterField;
    private SalesSystemDAO dao;
    private String[] acceptedFormats = {"d/MM/yyyy","d.MM.yyyy"};
    private boolean isStartDateValid;
    private boolean isEndDateValid;

    public void initialize(SalesSystemDAO dao) {
        log.info("Initializing HistoryController");
        this.dao = dao;
        historyTableView.getSelectionModel().selectedItemProperty().addListener(
                (observable, oldValue, newValue) -> refreshCartItems(newValue));
        enableTableViewProductChanging();
        enableValidation();
        EnableDataCopy.installCopyPasteHandler(historyTableView);
        EnableDataCopy.installCopyPasteHandler(cartTableView);
        historyTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        cartTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    private void enableValidation() {
        Validation.setFor(productFilterField, StringUtils::isAlphanumericSpace);
        startDatePicker.getEditor().textProperty().addListener(
                (observable, oldValue, newValue) -> isDatePickerValid(startDatePicker, newValue));
        endDatePicker.getEditor().textProperty().addListener(
                (observable, oldValue, newValue) -> isDatePickerValid(endDatePicker, newValue));
        Validation.setFor(startDatePicker.getEditor(), p -> isStartDateValid);
        Validation.setFor(endDatePicker.getEditor(), p -> isEndDateValid);
    }

    private void isDatePickerValid(DatePicker datePicker, String input) {
        boolean valid = true;
        try {
            DateUtils.parseDateStrictly(input, acceptedFormats);
        } catch (ParseException ignored) {
            valid = false;
        }
        if (startDatePicker.equals(datePicker)) {
            isStartDateValid = valid;
        } else {
            isEndDateValid = valid;
        }
        validateShowBetweenDatesButton();
    }

    private void validateShowBetweenDatesButton() {
        boolean valid = isStartDateValid && isEndDateValid &&
                (startDatePicker.getValue().isBefore(endDatePicker.getValue()) ||
                        startDatePicker.getValue().isEqual(endDatePicker.getValue()));
        showBetweenDates.setDisable(!valid);
    }

    @Override
    public void update() {
        refreshHistoryItems(dao.findHistoryItems());
        historyTableView.getItems().removeAll(historyTableView.getItems().stream()
                .filter(historyItem -> historyItem.getItems().size() == 0).collect(Collectors.toList()));
    }

    private void refreshHistoryItems(List<HistoryItem> items) {
        historyTableView.setItems(new ObservableListWrapper<>(items));
        historyTableView.refresh();
        dailyIncomeField.setText(Double.toString(findCurrentDayIncome()) + " \u20AC");
    }

    private void refreshCartItems(HistoryItem item) {
        if (item != null) {
            refreshCartItems(item.getItems());
        }
    }

    private void refreshCartItems(List<SoldItem> items) {
        cartTableView.setItems(new ObservableListWrapper<>(items));
        cartTableView.refresh();
    }

    /**
     * Enables product data changing on the cart.
     */
    private void enableTableViewProductChanging() {
        TableRowExpanderColumn<SoldItem> column = new TableRowExpanderColumn<>(row ->
                new HistoryCartTableRow(row, historyTableView.getSelectionModel().getSelectedItem(), this));
        column.setCellFactory(param -> new HistoryCartToggleCell<>(column));
        cartTableView.getColumns().add(column);
    }

    private double findCurrentDayIncome() {
        log.info("Current day income updated");
        return dao.findHistoryItems().stream()
                .filter(historyItem -> historyItem.getDate().equals(LocalDate.now()))
                .mapToDouble(HistoryItem::getTotal).sum();
    }

    private void clearCartTable() {
        log.info("Cart table cleared");
        cartTableView.setItems(new ObservableListWrapper<>(new ArrayList<>()));
    }

    @FXML
    protected void handleShowBetweenDates() {
        log.info("Showing history between dates");
        LocalDate start = startDatePicker.getValue();
        LocalDate end = endDatePicker.getValue();
        String filter = productFilterField.getText();
        List<HistoryItem> items = dao.findHistoryInBetween(start, end, filter);
        if (items.isEmpty()) {
            clearCartTable();
        }
        refreshHistoryItems(items);
    }

    @FXML
    protected void handleShowAll() {
        log.info("Showing all history");
        String filter = productFilterField.getText();
        List<HistoryItem> items = dao.findHistoryItems(filter);
        if (items.isEmpty()) {
            clearCartTable();
        }
        refreshHistoryItems(items);
    }

    @FXML
    protected void handleShowLast10() {
        log.info("Showing 10 last history items");
        String filter = productFilterField.getText();
        List<HistoryItem> items = dao.find10LastHistoryItems(filter);
        if (items.isEmpty()) {
            clearCartTable();
        }
        refreshHistoryItems(items);
    }
}