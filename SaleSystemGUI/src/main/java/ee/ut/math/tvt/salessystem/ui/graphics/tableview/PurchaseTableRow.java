package ee.ut.math.tvt.salessystem.ui.graphics.tableview;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.data.SoldItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.ui.common.Validation;
import ee.ut.math.tvt.salessystem.ui.controllers.PurchaseController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.table.TableRowExpanderColumn;

public class PurchaseTableRow extends HBox {
    private static final Logger log = LogManager.getLogger(PurchaseTableRow.class);
    private final TableRowExpanderColumn.TableRowDataFeatures<SoldItem> row;
    private final PurchaseController controller;
    private final SalesSystemDAO dao;
    private final ShoppingCart cart;
    private final TextField textField = new TextField();
    private final Text stockField = new Text();
    private final Button saveButton = new Button("Save");
    private boolean isQuantityValid;

    /**
     * Creates editable table row with textfield, save and remove buttons.
     */
    public PurchaseTableRow(TableRowExpanderColumn.TableRowDataFeatures<SoldItem> row, PurchaseController controller,
                            SalesSystemDAO dao, ShoppingCart cart) {
        this.row = row;
        this.controller = controller;
        this.dao = dao;
        this.cart = cart;
        setSpacing(10.0);
        setPadding(new Insets(10.0));
        setAlignment(Pos.TOP_RIGHT);
        Button removeButton = new Button("Remove");
        setMargin(removeButton, new Insets(0.0, 30.0, 0.0, 0.0));
        textField.setPromptText("Insert new quantity");
        saveButton.setMnemonicParsing(false);
        saveButton.setOnMouseClicked(event -> handleSaving());
        removeButton.setMnemonicParsing(false);
        removeButton.setOnMouseClicked(event -> removeItem());
        getChildren().addAll(stockField, textField, saveButton, removeButton);
        updateStock();
        enableValidation();
    }

    private void enableValidation() {
        textField.textProperty().addListener((observable, oldValue, newValue) -> isQuantityValid());
        Validation.setFor(textField, p -> isQuantityValid);
    }

    /**
     * Checks if current item quantity is valid.
     */
    private void isQuantityValid() {
        String value = textField.getText();
        isQuantityValid = value.isEmpty() || (StringUtils.isNumeric(value) && Integer.parseInt(value) > 0) &&
                cart.hasEnoughStock(dao.findStockItem(row.getValue().getId()), Integer.parseInt(value));
        validateSaveButton();
    }

    private void validateSaveButton() {
        boolean valid = isQuantityValid && !textField.getText().isEmpty();
        saveButton.setDisable(!valid);
    }

    /**
     * Saves current item quantity to the cart.
     */
    private void handleSaving() {
        int quantity = Integer.parseInt(textField.getText());
        log.info("Changing " + row.getValue().getName() + " quantity to " + quantity);
        updateItem(quantity);
    }

    /**
     * Updates current item quantity on cart.
     */
    private void updateItem(int quantity) {
        row.getValue().setQuantity(quantity);
        controller.updateTotalCost();
        row.setExpanded(false);
    }

    /**
     * Removes current item from cart.
     */
    private void removeItem() {
        cart.removeItem(row.getValue());
        controller.updateTotalCost();
        controller.validateCart();
        row.setExpanded(false);
    }

    private void updateStock() {
        stockField.setText("Total stock: " + dao.findStockItem(row.getValue().getId()).getQuantity());
    }
}