package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.data.StockItem;
import ee.ut.math.tvt.salessystem.ui.common.EnableDataCopy;
import ee.ut.math.tvt.salessystem.ui.common.Fxml;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class WarehouseController extends AbstractController {
    private static final Logger log = LogManager.getLogger(WarehouseController.class);
    private SalesSystemDAO dao;
    @FXML private BorderPane borderPane;
    @FXML private TableView<StockItem> warehouseTableView;
    private AddViewController addViewController;
    private EditViewController editViewController;

    /**
     * Initializes this controller.
     *
     * @param dao to use
     */
    public void initialize(SalesSystemDAO dao) throws IOException {
        log.info("Initializing WarehouseController");
        this.dao = dao;
        warehouseTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        EnableDataCopy.installCopyPasteHandler(warehouseTableView);
        warehouseTableView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        switchView(Fxml.ADDVIEW);
        enableProductViewing();
    }

    /**
     * Enables showing product info based on selected table row.
     */
    private void enableProductViewing() {
        warehouseTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) {
                switchView(Fxml.ADDVIEW);
                addViewController.setItem(newValue);
            }
        });
    }

    /**
     * Updates tableView on opening tab.
     */
    @Override
    public void update() {
        refreshStockItems();
    }

    /**
     * Updates tableView on clicking refresh button.
     */
    @FXML
    private void refreshButtonClicked() {
        refreshStockItems();
        clearTableRowSelection();
    }

    /**
     * Updates tableView items.
     */
    private void refreshStockItems() {
        warehouseTableView.setItems(new ObservableListWrapper<>(dao.findStockItems()));
        warehouseTableView.refresh();
    }

    /**
     * Switches view based on given fxml enum.
     *
     * @param fxml type to use
     */
    public void switchView(Fxml fxml) {
        FXMLLoader loader = new FXMLLoader(fxml.getResource());
        try {
            Parent node = loader.load();
            switch (fxml) {
                case ADDVIEW:
                    addViewController = loader.getController();
                    addViewController.initialize(this, dao);
                    break;
                case EDITVIEW:
                    editViewController = loader.getController();
                    editViewController.initialize(this, dao);
                    break;
            }
            borderPane.setTop(node);
        } catch (IOException e) {
            log.error("Failed to load " + fxml.getFileName() + "!");
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    /**
     * Gets addView controller.
     */
    public AddViewController getAddViewController() {
        return addViewController;
    }

    /**
     * Gets editView controller.
     */
    public EditViewController getEditViewController() {
        return editViewController;
    }

    /**
     * Clears table selection and resets view.
     */
    public void clearTableRowSelection() {
        warehouseTableView.getSelectionModel().clearSelection();
        switchView(Fxml.ADDVIEW);
    }


}