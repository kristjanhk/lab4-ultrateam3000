package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.ui.common.Updateable;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public abstract class AbstractController implements Initializable, Updateable {

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    @Override
    public void update() {

    }
}
