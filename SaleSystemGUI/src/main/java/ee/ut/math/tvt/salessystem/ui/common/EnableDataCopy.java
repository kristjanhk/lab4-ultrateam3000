package ee.ut.math.tvt.salessystem.ui.common;

import javafx.event.EventHandler;
import javafx.scene.control.TableView;
import javafx.scene.input.*;

public class EnableDataCopy {

    /**
     * Copy/Paste keyboard event handler.
     * The handler uses the keyEvent's source for the clipboard data. The source must be of type TableView.
     */
    private static class TableKeyEventHandler implements EventHandler<KeyEvent> {
        KeyCodeCombination copyKeyCodeCombination = new KeyCodeCombination(KeyCode.C, KeyCombination.CONTROL_ANY);
        public void handle(final KeyEvent keyEvent) {
            if (copyKeyCodeCombination.match(keyEvent)) {
                if (keyEvent.getSource() instanceof TableView) {
                    copySelectionToClipboard((TableView<?>) keyEvent.getSource());
                    keyEvent.consume();
                }
            }
        }
    }
    /**
     * Install the keyboard handler:
     * CTRL + C = copy to clipboard
     *
     * @param table to use
     */
    public static void installCopyPasteHandler(TableView<?> table) {
        table.setOnKeyPressed(new TableKeyEventHandler());
    }

    /**
     * Get table selection and copy it to the clipboard.
     *
     * @param table to use
     */
    private static void copySelectionToClipboard(TableView<?> table) {
        final ClipboardContent clipboardContent = new ClipboardContent();
        clipboardContent.putString(table.getSelectionModel().getSelectedItems().toString().replaceAll("\\[|]|,", ""));
        Clipboard.getSystemClipboard().setContent(clipboardContent);
    }
}
