package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.data.StockItem;
import ee.ut.math.tvt.salessystem.ui.common.Fxml;
import ee.ut.math.tvt.salessystem.ui.common.Validation;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import java.util.Objects;
import java.util.stream.Collectors;

public class AddViewController extends AbstractController {
    private static final Logger log = LogManager.getLogger(AddViewController.class);
    private WarehouseController controller;
    private SalesSystemDAO dao;
    @FXML private TitledPane titlePane;
    @FXML private TextField barcodeField;
    @FXML private TextField quantityField;
    @FXML private TextField nameField;
    @FXML private TextField priceField;
    @FXML private Button button;
    private AutoCompletionBinding<String> autoBarcodeField;
    private AutoCompletionBinding<String> autoNameField;
    private boolean isBarCodeValid;
    private boolean isQuantityValid;
    private boolean isNameValid;
    private boolean isPriceValid;
    private StockItem currentItem;

    /**
     * Initializes this controller.
     *
     * @param controller to use
     * @param dao        to use
     */
    public void initialize(WarehouseController controller, SalesSystemDAO dao) {
        log.info("Initializing AddViewController");
        this.controller = controller;
        this.dao = dao;
        button.setDisable(true);
        enableBarCodeFieldAutoCompletion();
        enableNameFieldAutoCompletion();
        enableFieldsValidation();
    }

    /**
     * Stores current item and populates fields.
     *
     * @param item to use
     */
    public void setItem(StockItem item) {
        currentItem = item;
        fillProductInfo(item, true, true);
    }

    /**
     * Handles button's click action.
     */
    @FXML
    private void handleButtonAction() {
        switch (button.getText()) {
            case "Add product":
                StockItem stockItem = new StockItem(Long.parseLong(barcodeField.getText()), nameField.getText(),
                        Double.parseDouble(priceField.getText()), Integer.parseInt(quantityField.getText()));
                dao.beginTransaction();
                dao.saveStockItem(stockItem);
                dao.commitTransaction();
                resetView();
                controller.update();
                break;
            case "Edit product":
                controller.switchView(Fxml.EDITVIEW);
                controller.getEditViewController().setItem(currentItem);
                break;
        }
    }

    /**
     * Enables fields validation.
     */
    private void enableFieldsValidation() {
        barcodeField.textProperty().addListener((observable, oldValue, newValue) -> isBarCodeValid());
        quantityField.textProperty().addListener((observable, oldValue, newValue) -> isQuantityValid());
        nameField.textProperty().addListener((observable, oldValue, newValue) -> isNameValid());
        priceField.textProperty().addListener((observable, oldValue, newValue) -> isPriceValid());
        Validation.setFor(barcodeField, p -> validateBarCodeField());
        Validation.setFor(quantityField, p -> isQuantityValid);
        Validation.setFor(nameField, p -> isNameValid);
        Validation.setFor(priceField, p -> isPriceValid);
    }

    /**
     * Checks if barcode field is valid.
     */
    private void isBarCodeValid() {
        isBarCodeValid = StringUtils.isNumeric(barcodeField.getText()) && Long.parseLong(barcodeField.getText()) > 0;
        validateButton();
    }

    /**
     * Checks if quantity field is valid.
     */
    private void isQuantityValid() {
        isQuantityValid = StringUtils.isNumeric(quantityField.getText()) &&
                Integer.parseInt(quantityField.getText()) >= 0;
        validateButton();
    }

    /**
     * Checks if name field is valid.
     */
    private void isNameValid() {
        isNameValid = !StringUtils.isNumeric(nameField.getText());
        validateButton();
    }

    /**
     * Checks if price field is valid.
     */
    private void isPriceValid() {
        String value = priceField.getText();
        boolean isValid = value.matches("(\\d+((\\.|,)\\d{1,2})?)|^$");
        if (isValid && !value.isEmpty()) {
            value = value.replace(",", ".");
            isValid = Double.parseDouble(value) >= 0;
        }
        isPriceValid = isValid || value.isEmpty();
        validateButton();
    }

    /**
     * Validates button.
     */
    private void validateButton() {
        boolean isValid = isBarCodeValid && isQuantityValid && isNameValid && isPriceValid;
        button.setDisable(!isValid);
    }

    private boolean validateBarCodeField() {
        String value = barcodeField.getText();
        boolean valid = isBarCodeValid;
        if (isBarCodeValid) {
            StockItem item = dao.findStockItem(Long.parseLong(value));
            valid = item == null || Objects.equals(currentItem, item);
        }
        return valid || value.isEmpty();
    }

    /**
     * Enables barcode field automatic completion.
     */
    private void enableBarCodeFieldAutoCompletion() {
        if (autoBarcodeField != null) {
            autoBarcodeField.dispose();
        }
        autoBarcodeField = TextFields.bindAutoCompletion(barcodeField, param -> dao.findStockItems().stream()
                .map(item -> String.valueOf(item.getId()))
                .filter(itemName -> itemName.contains(param.getUserText()))
                .collect(Collectors.toList()));
        autoBarcodeField.setOnAutoCompleted(event -> fillProductInfoByBarCode());
    }

    /**
     * Enables automatic completion of name field.
     */
    private void enableNameFieldAutoCompletion() {
        if (autoNameField != null) {
            autoNameField.dispose();
        }
        autoNameField = TextFields.bindAutoCompletion(nameField, param -> dao.findStockItems().stream()
                .map(StockItem::getName)
                .filter(itemName -> itemName.toLowerCase().contains(param.getUserText().toLowerCase()))
                .collect(Collectors.toList()));
        autoNameField.setOnAutoCompleted(event -> fillProductInfoByName());
    }

    /**
     * Fills fields based on barcode.
     */
    private void fillProductInfoByBarCode() {
        currentItem = dao.findStockItem(Long.parseLong(barcodeField.getText()));
        fillProductInfo(currentItem, false, true);
    }

    /**
     * Fills fields based on name.
     */
    private void fillProductInfoByName() {
        currentItem = dao.findStockItem(nameField.getText());
        fillProductInfo(currentItem, true, false);
    }

    /**
     * Fills fields based on given item.
     *
     * @param item to use
     * @param fillBarCode if should fill the barcode field
     * @param fillName if should fill the name field
     */
    private void fillProductInfo(StockItem item, boolean fillBarCode, boolean fillName) {
        disableFields(true);
        quantityField.setText(String.valueOf(item.getQuantity()));
        priceField.setText(String.valueOf(item.getPrice()));
        if (fillBarCode) {
            barcodeField.setText(String.valueOf(item.getId()));
            resetNameField();
        }
        if (fillName) {
            nameField.setText(item.getName());
            resetBarCodeField();
        }
    }

    private void resetBarCodeField() {
        String value = barcodeField.getText();
        barcodeField.setText("");
        barcodeField.setText(value);
    }

    private void resetNameField() {
        String value = nameField.getText();
        nameField.setText("");
        nameField.setText(value);
    }

    /**
     * Disables fields.
     *
     * @param disable condition
     */
    private void disableFields(boolean disable) {
        titlePane.setText(disable ? "Product info" : "Add product");
        button.setText(disable ? "Edit product" : "Add product");
        quantityField.setDisable(disable);
        priceField.setDisable(disable);
    }

    /**
     * Clears tableView selection, which in turn switches out this view.
     */
    private void resetView() {
        controller.clearTableRowSelection();
    }
}
