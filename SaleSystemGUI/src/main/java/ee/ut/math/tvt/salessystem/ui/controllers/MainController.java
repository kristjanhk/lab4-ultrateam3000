package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.ui.common.Fxml;
import ee.ut.math.tvt.salessystem.ui.common.Updateable;
import ee.ut.math.tvt.salessystem.ui.graphics.ResizeHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.WindowEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

public class MainController extends AbstractController {
    private static final Logger log = LogManager.getLogger(MainController.class);
    @FXML private Button maximizeButton;
    @FXML private BorderPane borderPane;
    @FXML private TabPane tabPane;
    @FXML private Tab posTab;
    @FXML private Tab warehouseTab;
    @FXML private Tab historyTab;
    @FXML private Tab teamTab;
    private Parent parent;
    private PurchaseController purchaseController;
    private WarehouseController warehouseController;
    private HistoryController historyController;
    private TeamController teamController;
    private ResizeHandler resizeHandler;
    private Stage stage;
    private double stageXOffset;
    private double stageYOffset;

    /**
     * Loads in a new node based on given fxml enum and gets this controller.
     *
     * @param fxml to use
     */
    public static MainController newInstance(Fxml fxml) throws IOException {
        log.info("Initializing MainController");
        FXMLLoader loader = new FXMLLoader(fxml.getResource());
        Parent parent = loader.load();
        MainController self = loader.getController();
        self.setParent(parent);
        return self;
    }

    /**
     * Gets this controller's node.
     */
    public Parent getParent() {
        return parent;
    }

    /**
     * Sets this controller's node.
     */
    private void setParent(Parent parent) {
        this.parent = parent;
    }

    /**
     * Initializes this controllers content:
     *
     * Sets given stage as undecorated and assigns resizeListener,
     * loads in tabs' contents,
     * enables tab refreshing/updating on opening.
     *
     * @param scene to use
     * @param stage to use
     * @param dao to use
     * @param cart to use
     */
    public void initializeContent(Scene scene, Stage stage, SalesSystemDAO dao, ShoppingCart cart) throws IOException {
        log.info("Initializing Scene content");
        this.stage = stage;
        stage.initStyle(StageStyle.UNDECORATED);
        resizeHandler = new ResizeHandler(stage);
        borderPane.prefHeightProperty().bind(scene.heightProperty());
        borderPane.prefWidthProperty().bind(scene.widthProperty());
        posTab.setContent(loadContent(Fxml.PURCHASE, dao, cart));
        warehouseTab.setContent(loadContent(Fxml.WAREHOUSE, dao, cart));
        historyTab.setContent(loadContent(Fxml.HISTORY, dao, cart));
        teamTab.setContent(loadContent(Fxml.TEAM, dao, cart));
        Updateable[] updateables = new Updateable[]{
                purchaseController, warehouseController, historyController, teamController};
        tabPane.getSelectionModel().selectedIndexProperty().addListener(
                (observable, oldValue, newValue) -> updateables[newValue.intValue()].update());
        resizeHandler.addResizeListener(scene);
        setMaximizeButton();
    }

    /**
     * Loads in node based on given fxml enum and initializes its' controller.
     *
     * @param fxml type to use
     * @param dao to use
     * @param cart to use
     */
    private Node loadContent(Fxml fxml, SalesSystemDAO dao, ShoppingCart cart) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(fxml.getResource());
        Node node = fxmlLoader.load();
        switch (fxml) {
            case PURCHASE:
                purchaseController = fxmlLoader.getController();
                purchaseController.initialize(dao, cart);
                break;
            case WAREHOUSE:
                warehouseController = fxmlLoader.getController();
                warehouseController.initialize(dao);
                break;
            case HISTORY:
                historyController = fxmlLoader.getController();
                historyController.initialize(dao);
                break;
            case TEAM:
                teamController = fxmlLoader.getController();
                teamController.initialize();
                break;
        }
        return node;
    }

    /**
     * Handles window location changing.
     */
    @FXML
    private void handleStageLocationOffset(MouseEvent event) {
        if (Cursor.DEFAULT.equals(resizeHandler.getCursor()) && !stage.isMaximized()) {
            setStageOffsets(event.getScreenX(), event.getScreenY());
        }
    }

    /**
     * Handles window location changing.
     */
    @FXML
    private void handleStageLocationDrag(MouseEvent event) {
        if (Cursor.DEFAULT.equals(resizeHandler.getCursor()) && !stage.isMaximized()) {
            setStageXY(event.getScreenX(), event.getScreenY());
        }
    }

    /**
     * Handles window minimizing.
     */
    @FXML
    private void handleStageMinimize() {
        stage.setIconified(true);
    }

    /**
     * Handles window maximizing.
     */
    @FXML
    private void handleStageMaximize() {
        stage.setMaximized(!stage.isMaximized());
        setMaximizeButton();
    }

    /**
     * Handles window closing.
     */
    @FXML
    private void handleStageClose() {
        stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
    }

    /**
     * Toggles restore/maximize button text.
     */
    private void setMaximizeButton() {
        if (maximizeButton != null) {
            maximizeButton.setText(stage.isMaximized() ? "\u2193" : "\u2191");
        }
    }

    /**
     * Sets stage location coordinates.
     */
    private void setStageXY(double eventX, double eventY) {
        stage.setX(eventX + stageXOffset);
        stage.setY(eventY + stageYOffset);
    }

    /**
     * Sets stage location coordinate offsets.
     */
    private void setStageOffsets(double eventX, double eventY) {
        stageXOffset = stage.getX() - eventX;
        stageYOffset = stage.getY() - eventY;
    }
}
