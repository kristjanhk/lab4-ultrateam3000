package ee.ut.math.tvt.salessystem.ui.controllers;

import com.sun.javafx.collections.ObservableListWrapper;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.data.SoldItem;
import ee.ut.math.tvt.salessystem.data.StockItem;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.ui.common.Validation;
import ee.ut.math.tvt.salessystem.ui.graphics.tableview.PurchaseTableRow;
import ee.ut.math.tvt.salessystem.ui.graphics.tableview.PurchaseToggleCell;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import org.apache.commons.lang3.StringUtils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.table.TableRowExpanderColumn;
import org.controlsfx.control.textfield.AutoCompletionBinding;
import org.controlsfx.control.textfield.TextFields;

import java.util.stream.Collectors;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu). Consists of the purchase menu,
 * current purchase dialog and shopping cart table.
 */
public class PurchaseController extends AbstractController {
    private static final Logger log = LogManager.getLogger(PurchaseController.class);
    private SalesSystemDAO dao;
    private ShoppingCart cart;
    @FXML private Button newPurchase;
    @FXML private Button confirmPurchase;
    @FXML private Button cancelPurchase;
    @FXML private Text barCodeField;
    @FXML private TextField quantityField;
    @FXML private TextField nameField;
    @FXML private Text priceField;
    @FXML private Text stockField;
    @FXML private Button addItemButton;
    @FXML private TableView<SoldItem> purchaseTableView;
    @FXML private Text textTotalCost;
    @FXML private Text textTotalCostText;
    @FXML private TextField changeField;
    @FXML private Button calculateChangeButton;
    private AutoCompletionBinding<String> autoNameField;
    private boolean isQuantityValid;
    private boolean isNameValid;
    private boolean isChangeValid;

    /**
     * Initializes this controller.
     *
     * @param dao to use
     * @param cart to use
     */
    public void initialize(SalesSystemDAO dao, ShoppingCart cart) {
        log.info("Initializing PurchaseController");
        this.dao = dao;
        this.cart = cart;
        enableTableViewQuantityChanging();
        enableFieldsValidation();
        nameField.textProperty().addListener((observable, oldValue, newValue) -> tryUpdatingBarCodeField());
        disableFields(true);
        cancelPurchase.setDisable(true);
        confirmPurchase.setDisable(true);
        nameField.setOnMousePressed(event -> autoNameField.setUserInput(""));
    }

    /**
     * Tries to update barcode field based on name field input.
     */
    private void tryUpdatingBarCodeField() {
        if (isNameValid && !nameField.getText().isEmpty()) {
            StockItem item = dao.findStockItem(nameField.getText());
            String id = String.valueOf(item.getId());
            if (!barCodeField.getText().equals(id)) {
                barCodeField.setText(id);
                priceField.setText(String.valueOf(item.getPrice()) + " \u20AC");
                updateStockField();
            }
        }
    }

    /**
     * Enables product quantity changing from shopping cart.
     */
    private void enableTableViewQuantityChanging() {
        purchaseTableView.setItems(new ObservableListWrapper<>(cart.getAll()));
        TableRowExpanderColumn<SoldItem> column = new TableRowExpanderColumn<>(row ->
                new PurchaseTableRow(row, this, dao, cart));
        column.setCellFactory(param -> new PurchaseToggleCell<>(column));
        purchaseTableView.getColumns().add(column);
    }

    /**
     * Enables automatic completion of name field.
     */
    private void enableNameFieldAutoCompletion() {
        if (autoNameField != null) {
            autoNameField.dispose();
        }
        autoNameField = TextFields.bindAutoCompletion(nameField, param -> dao.findStockItems().stream()
                .filter(item -> item.getName().toLowerCase().contains(param.getUserText().toLowerCase()))
                .filter(item -> !item.getName().toLowerCase().equals(param.getUserText().toLowerCase()))
                .filter(item -> item.getQuantity() > 0)
                .filter(item -> cart.hasEnoughTotalStock(item, cart.getItemQuantityIfExists(item)))
                .map(StockItem::getName)
                .collect(Collectors.toList()));
        autoNameField.setOnAutoCompleted(event -> fillInputsByNameField());
    }

    /**
     * Enables quantity, name and change fields validation.
     */
    private void enableFieldsValidation() {
        quantityField.textProperty().addListener((observable, oldValue, newValue) -> isQuantityValid());
        nameField.textProperty().addListener((observable, oldValue, newValue) -> isNameValid());
        changeField.textProperty().addListener((observable, oldValue, newValue) -> isChangeValid());
        Validation.setFor(quantityField, p -> isQuantityValid);
        Validation.setFor(nameField, p -> isNameValid);
        Validation.setFor(changeField, p -> isChangeValid);
    }

    /**
     * Enables add button when name and quantity fields are valid.
     */
    private void validateAddButton() {
        boolean isValid = isQuantityValid && isNameValid && !nameField.getText().isEmpty();
        addItemButton.setDisable(!isValid);
    }

    /**
     * Enables change button when change field is valid.
     */
    private void validateChangeButton() {
        boolean isValid = isChangeValid && !changeField.getText().isEmpty();
        calculateChangeButton.setDisable(!isValid);
    }

    /**
     * Checks if quantity field is valid and validates add button.
     */
    private void isQuantityValid() {
        boolean valid = StringUtils.isNumeric(quantityField.getText());
        boolean hasEnoughStock = false;
        if (valid && isNameValid) {
            int quantity = Integer.parseInt(quantityField.getText());
            hasEnoughStock = quantity > 0 && cart.hasEnoughTotalStock(dao.findStockItem(nameField.getText()), quantity);
        }
        isQuantityValid = valid && hasEnoughStock;
        validateAddButton();
    }

    /**
     * Checks if name field is valid and validates add button.
     */
    private void isNameValid() {
        isNameValid = nameField.getText().isEmpty() || dao.findStockItem(nameField.getText()) != null;
        validateAddButton();
        if (isNameValid && !nameField.getText().isEmpty()) {
            isQuantityValid();
        }
    }

    /**
     * Checks if change field is valid and validates check button.
     */
    private void isChangeValid() {
        String value = changeField.getText();
        boolean isValid = value.matches("(\\d+((\\.|,)\\d{2})?)|^$");
        value = value.replace(",", ".");
        if (isValid && !value.isEmpty()) {
            isValid = Double.parseDouble(value) >= cart.getTotalCost();
        }
        isChangeValid = isValid || value.isEmpty();
        validateChangeButton();
    }

    /**
     * Validates cart and enables sale confirming.
     */
    public void validateCart() {
        confirmPurchase.setDisable(cart.getAll().size() == 0);
    }

    /**
     * Event handler for the <code>new purchase</code> event.
     */
    @FXML
    private void newPurchaseButtonClicked() {
        log.info("New sale process started");
        enableInputs();
        enableNameFieldAutoCompletion();
        addItemButton.setDisable(true);
        calculateChangeButton.setDisable(true);
    }

    /**
     * Event handler for the <code>cancel purchase</code> event.
     */
    @FXML
    private void cancelPurchaseButtonClicked() {
        log.info("Sale cancelled");
        cart.cancelCurrentPurchase();
        disableInputs();
        resetTotalCost();
        purchaseTableView.refresh();
    }

    /**
     * Event handler for the <code>submit purchase</code> event.
     */
    @FXML
    private void confirmPurchaseButtonClicked() {
        log.info("Sale complete");
        log.debug("Contents of the current basket:\n" + cart.getAll());
        cart.submitCurrentPurchase();
        disableInputs();
        resetTotalCost();
        purchaseTableView.refresh();
    }

    /**
     * Calculates change based on given amount and cart total cost.
     */
    @FXML
    private void calculateChange() {
        calculateChangeButton.setText("Change: " + cart.calculateChange(
                Double.parseDouble(changeField.getText().replaceAll(",", ".")), cart.getTotalCost()) + " \u20AC");
    }

    /**
     * Enables sale inputs.
     */
    private void enableInputs() {
        resetFields();
        disableFields(false);
        addItemButton.setDisable(false);
        cancelPurchase.setDisable(false);
        confirmPurchase.setDisable(true);
        newPurchase.setDisable(true);
    }

    /**
     * Disables sale inputs.
     */
    private void disableInputs() {
        resetFields();
        cancelPurchase.setDisable(true);
        confirmPurchase.setDisable(true);
        newPurchase.setDisable(false);
        disableFields(true);
    }

    /**
     * Sets whether or not the product component is enabled.
     *
     * @param disable set field state
     */
    private void disableFields(boolean disable) {
        barCodeField.setDisable(disable);
        quantityField.setDisable(disable);
        nameField.setDisable(disable);
        priceField.setDisable(disable);
        calculateChangeButton.setDisable(disable);
        changeField.setDisable(disable);
    }

    /**
     * Reset dialog fields.
     */
    private void resetFields() {
        barCodeField.setText("");
        quantityField.setText("1");
        isQuantityValid = true;
        nameField.setText("");
        priceField.setText("");
        stockField.setText("");
        changeField.setText("");
    }

    /**
     * Fills fields based on chosen product name.
     */
    private void fillInputsByNameField() {
        StockItem stockItem = dao.findStockItem(nameField.getText());
        barCodeField.setText(String.valueOf(stockItem.getId()));
        priceField.setText(String.valueOf(stockItem.getPrice()) + " \u20AC");
        updateStockField();
    }

    /**
     * Add new item to the cart.
     */
    @FXML
    private void addItemEventHandler() {
        StockItem stockItem = dao.findStockItem(Long.parseLong(barCodeField.getText()));
        int quantity = Integer.parseInt(quantityField.getText());
        cart.addItem(new SoldItem(stockItem, quantity));
        updateTotalCost();
        log.info("Added " + quantity + "x " + stockItem.getName() + " to shopping cart");
        purchaseTableView.refresh();
        resetFields();
        validateCart();
    }

    /**
     * Updates stock field based warehouse and cart quantities.
     */
    private void updateStockField() {
        String text = "In stock: ";
        if (isNameValid) {
            StockItem item = dao.findStockItem(nameField.getText());
            SoldItem cartItem = cart.getItem(item.getId());
            text += item.getQuantity() - (cartItem != null ? cartItem.getQuantity() : 0);
        }
        stockField.setText(text);
    }

    /**
     * Hides total cost fields.
     */
    private void resetTotalCost() {
        textTotalCost.setText("");
        textTotalCostText.setText("");
        changeField.setText("");
        calculateChangeButton.setText("calculate change");
    }

    /**
     * Shows current carts' total cost.
     */
    public void updateTotalCost() {
        textTotalCostText.setText("Total cost");
        textTotalCost.setText(String.valueOf(cart.getTotalCost()) + " \u20AC");
    }

}