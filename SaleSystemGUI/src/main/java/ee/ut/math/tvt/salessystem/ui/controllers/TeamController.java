package ee.ut.math.tvt.salessystem.ui.controllers;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Text;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Properties;

public class TeamController extends AbstractController {
    private static final Logger log = LogManager.getLogger(TeamController.class);
    @FXML private Text textMembers;
    @FXML private Text textTeamLeaderEmail;
    @FXML private Text textTeamLeader;
    @FXML private Text textTeamName;
    @FXML private ImageView imageViewLogo;

    public void initialize() throws IOException {
        log.info("Initializing TeamController");
        Properties properties = new Properties();
        try (InputStream in = new BufferedInputStream(
                getClass().getResourceAsStream("/ee/ut/math/tvt/salessystem/application.properties"))) {
            properties.load(in);
            textMembers.setText(properties.getProperty("teamMember1") + ", " + properties.getProperty("teamMember2"));
            textTeamLeaderEmail.setText(properties.getProperty("teamLeaderEmail"));
            textTeamLeader.setText(properties.getProperty("teamLeader"));
            textTeamName.setText(properties.getProperty("teamName"));
            imageViewLogo.setImage(new Image(getClass().getResourceAsStream(properties.getProperty("logo"))));
        } catch (IOException e) {
            log.error("Application.properties file is missing!");
            throw e;
        }
    }
}