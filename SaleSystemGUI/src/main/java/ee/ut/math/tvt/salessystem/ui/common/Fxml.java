package ee.ut.math.tvt.salessystem.ui.common;

import java.net.URL;

public enum Fxml {
    TABMANAGER("TabManager"),
    PURCHASE("PurchaseTab"),
    WAREHOUSE("WarehouseTab"),
    HISTORY("HistoryTab"),
    TEAM("TeamTab"),
    ADDVIEW("AddView"),
    EDITVIEW("EditView");

    private final String fxml;

    Fxml(String fxml) {
        this.fxml = fxml;
    }

    public String getFileName() {
        return fxml + ".fxml";
    }

    public URL getResource() {
        return getClass().getResource("/ee/ut/math/tvt/salessystem/ui/" + getFileName());
    }
}
