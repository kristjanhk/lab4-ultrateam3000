package ee.ut.math.tvt.salessystem.ui.graphics.tableview;

import ee.ut.math.tvt.salessystem.data.HistoryItem;
import ee.ut.math.tvt.salessystem.data.SoldItem;
import ee.ut.math.tvt.salessystem.ui.common.Validation;
import ee.ut.math.tvt.salessystem.ui.controllers.HistoryController;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.controlsfx.control.table.TableRowExpanderColumn;

public class HistoryCartTableRow extends HBox {
    private static final Logger log = LogManager.getLogger(HistoryCartTableRow.class);
    private final TableRowExpanderColumn.TableRowDataFeatures<SoldItem> row;
    private final HistoryItem historyItem;
    private final TextField textFieldQuantity = new TextField();
    private final TextField textFieldPrice = new TextField();
    private final Button saveButton = new Button("Save");
    private HistoryController controller;
    private boolean isPriceValid = true;
    private boolean isQuantityValid = true;

    /**
     * Creates editable table row with textfield, save and remove buttons.
     */
    public HistoryCartTableRow(TableRowExpanderColumn.TableRowDataFeatures<SoldItem> row, HistoryItem historyItem,
                               HistoryController controller) {
        this.row = row;
        this.historyItem = historyItem;
        this.controller = controller;
        setSpacing(10.0);
        setPadding(new Insets(10.0));
        setAlignment(Pos.TOP_LEFT);
        Button removeButton = new Button("Remove");
        setMargin(removeButton, new Insets(0.0, 0.0, 0.0, 20.0));
        textFieldQuantity.setPromptText("Insert new quantity");
        textFieldPrice.setPromptText("Insert new price");
        saveButton.setMnemonicParsing(false);
        saveButton.setOnMouseClicked(event -> handleSaving());
        removeButton.setMnemonicParsing(false);
        removeButton.setOnMouseClicked(event -> removeItem());
        getChildren().addAll(textFieldPrice, textFieldQuantity, saveButton, removeButton);
        enableValidation();
    }

    private void enableValidation() {
        textFieldPrice.textProperty().addListener((observable, oldValue, newValue) -> isPriceValid());
        textFieldQuantity.textProperty().addListener((observable, oldValue, newValue) -> isQuantityValid());
        Validation.setFor(textFieldPrice, p -> isPriceValid);
        Validation.setFor(textFieldQuantity, p -> isQuantityValid);
    }

    /**
     * Checks if price field is valid.
     */
    private void isPriceValid() {
        String value = textFieldPrice.getText();
        boolean isValid = value.matches("(\\d+((\\.|,)\\d{1,2})?)");
        if (isValid && !value.isEmpty()) {
            value = value.replace(",", ".");
            isValid = Double.parseDouble(value) >= 0;
        }
        isPriceValid = isValid || value.isEmpty();
        validateSaveButton();
    }

    /**
     * Checks if current item quantity is valid.
     */
    private void isQuantityValid() {
        String value = textFieldQuantity.getText();
        isQuantityValid = value.isEmpty() || (StringUtils.isNumeric(value) && Integer.parseInt(value) > 0);
        validateSaveButton();
    }

    private void validateSaveButton() {
        boolean valid = isPriceValid && isQuantityValid;
        saveButton.setDisable(!valid);
    }

    /**
     * Saves current item quantity to the cart.
     */
    private void handleSaving() {
        int quantity = row.getValue().getQuantity();
        double price = row.getValue().getPrice();
        if (textFieldQuantity.getText().length() > 0) {
            quantity = Integer.parseInt(textFieldQuantity.getText());
        }
        if (textFieldPrice.getText().length() > 0) {
            price = Double.parseDouble(textFieldPrice.getText());
        }
        log.info("Changing " + row.getValue().getName() + " quantity to " + quantity);
        log.info("Changing " + row.getValue().getName() + " price " + price);
        updateItem(quantity, price);
    }

    /**
     * Updates current item quantity and price on cart.
     */
    private void updateItem(int quantity, double price) {
        row.getValue().setQuantity(quantity);
        row.getValue().setPrice(price);
        controller.update();
        row.setExpanded(false);
    }

    /**
     * Removes current item from cart.
     */
    private void removeItem() {
        historyItem.removeItem(row.getValue());
        controller.update();
        row.setExpanded(false);
    }
}