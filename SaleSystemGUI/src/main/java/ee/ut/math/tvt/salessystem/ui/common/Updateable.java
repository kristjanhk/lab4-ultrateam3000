package ee.ut.math.tvt.salessystem.ui.common;

public interface Updateable {
    void update();
}
