package ee.ut.math.tvt.salessystem.ui.graphics.tableview;

import ee.ut.math.tvt.salessystem.data.SoldItem;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import org.controlsfx.control.table.TableRowExpanderColumn;

public class HistoryCartToggleCell<S> extends TableCell<S, Boolean> {
    private final Button button = new Button();

    public HistoryCartToggleCell(TableRowExpanderColumn<SoldItem> column) {
        button.setOnAction(event -> column.toggleExpanded(getIndex()));
    }

    @Override
    protected void updateItem(Boolean expanded, boolean empty) {
        super.updateItem(expanded, empty);
        if (expanded == null || empty) {
            setGraphic(null);
        } else {
            button.setText(expanded ? "Close" : "Edit");
            setGraphic(button);
        }
    }
}
