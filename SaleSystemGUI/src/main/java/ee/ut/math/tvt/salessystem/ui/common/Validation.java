package ee.ut.math.tvt.salessystem.ui.common;

import javafx.scene.Node;
import javafx.scene.control.Control;
import org.controlsfx.validation.Severity;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.StyleClassValidationDecoration;

import java.util.function.Predicate;

public class Validation {

    public static void setFor(Node node, Predicate<String> predicate) {
        ValidationSupport vs = createSupport();
        vs.registerValidator((Control) node, true,
                Validator.createPredicateValidator(predicate, "error", Severity.ERROR));
    }

    private static ValidationSupport createSupport()  {
        ValidationSupport validationSupport = new ValidationSupport();
        validationSupport.setValidationDecorator(
                new StyleClassValidationDecoration("validationError", "validationWarning"));
        return validationSupport;
    }
}