package ee.ut.math.tvt.salessystem.ui.controllers;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.data.StockItem;
import ee.ut.math.tvt.salessystem.ui.common.Validation;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EditViewController extends AbstractController {
    private static final Logger log = LogManager.getLogger(EditViewController.class);
    private WarehouseController controller;
    private SalesSystemDAO dao;
    @FXML private TextField name;
    @FXML private Text totalQuantity;
    @FXML private TextField quantity;
    @FXML private Button removeStock;
    @FXML private Button addStock;
    @FXML private Button changeAmount;
    @FXML private Text barcode;
    @FXML private Button remove;
    private StockItem stockItem;
    private int insertedQuantity;
    private int addedQuantity;
    private int initialQuantity;
    private boolean isActive;
    private boolean isQuantityValid;

    /**
     * Initializes this controller.
     *
     * @param controller to use
     * @param dao        to use
     */
    public void initialize(WarehouseController controller, SalesSystemDAO dao) {
        log.info("Initializing EditViewController");
        this.controller = controller;
        this.dao = dao;
        enableValidation();
        quantity.textProperty().addListener((observable, oldValue, newValue) -> enableQuantityUpdating());
        disableChangeButtons(true);
    }

    private void enableValidation() {
        Validation.setFor(name, this::isNameValid);
        quantity.textProperty().addListener((observable, oldValue, newValue) -> isQuantityValid(newValue));
        Validation.setFor(quantity, p -> isQuantityValid);
    }

    private boolean isNameValid(String input) {
        StockItem item = dao.findStockItem(input);
        return !StringUtils.isWhitespace(input) &&
                StringUtils.isAsciiPrintable(input) &&
                (item == null || item.equals(stockItem));
    }

    private void isQuantityValid(String input) {
        isQuantityValid = input.isEmpty() || StringUtils.isNumeric(input);
        disableChangeButtons(!isQuantityValid);
    }

    private void enableQuantityUpdating() {
        String value = quantity.getText();
        boolean valid = isQuantityValid && !value.isEmpty();
        addedQuantity = valid ? Integer.parseInt(value) : -1;
        disableChangeButtons(!valid || addedQuantity == 0, !valid || addedQuantity == 0, !valid);
        insertedQuantity = addedQuantity;
        checkRemove();
        addStock.setText("Add " + (valid ? value + " " : "") + "to stock");
        removeStock.setText("Remove " + (valid ? value + " " : "") + "from stock");
        changeAmount.setText("Change quantity to " + (valid ? value : ""));
    }

    private void disableChangeButtons(boolean all) {
        disableChangeButtons(all, all, all);
    }

    private void disableChangeButtons(boolean add, boolean remove, boolean change) {
        addStock.setDisable(add);
        removeStock.setDisable(remove);
        changeAmount.setDisable(change);
    }

    private void checkRemove() {
        if (stockItem.getQuantity() < addedQuantity) {
            removeStock.setDisable(true);
        } else if (stockItem.getQuantity() >= addedQuantity && isQuantityValid && !quantity.getText().isEmpty()) {
            removeStock.setDisable(false);
        }
    }

    /**
     * Stores current item and populates fields.
     *
     * @param item to use
     */
    public void setItem(StockItem item) {
        stockItem = item;
        initialQuantity = item.getQuantity();
        barcode.setText("Barcode: " + String.valueOf(item.getId()));
        name.setText(item.getName());
        totalQuantity.setText(String.valueOf(item.getQuantity()));
    }

    @FXML
    public void save() {
        dao.beginTransaction();
        if (isActive) {
            dao.removeStockItem(stockItem);
        } else {
            stockItem.setName(name.getText());
            dao.replaceStockItem(stockItem);
        }
        dao.commitTransaction();
        controller.update();
        controller.clearTableRowSelection();
    }


    /**
     * Handles cancel button's click action, which clears table selection and in turn resets view.
     */
    @FXML
    public void handleCancel() {
        log.info("Editing canceled");
        stockItem.setQuantity(initialQuantity);
        controller.clearTableRowSelection();
    }

    @FXML
    public void addAmount() {
        log.info(insertedQuantity + " items added");
        stockItem.setQuantity(stockItem.getQuantity() + insertedQuantity);
        totalQuantity.setText(String.valueOf(stockItem.getQuantity()));
        checkRemove();
    }

    @FXML
    public void removeAmount() {
        log.info(insertedQuantity + " items removed");
        stockItem.setQuantity(stockItem.getQuantity() - insertedQuantity);
        totalQuantity.setText(String.valueOf(stockItem.getQuantity()));
        checkRemove();
    }

    @FXML
    public void changeAmount() {
        log.info("Quantity changed to " + insertedQuantity);
        stockItem.setQuantity(insertedQuantity);
        totalQuantity.setText(String.valueOf(stockItem.getQuantity()));
        checkRemove();
    }

    private void setEverythingDisabled(boolean value) {
        name.setDisable(value);
        totalQuantity.setDisable(value);
        quantity.setDisable(value);
        removeStock.setDisable(value);
        addStock.setDisable(value);
        changeAmount.setDisable(value);
        barcode.setDisable(value);
        if (!value) {
            checkRemove();
        }
    }

    @FXML
    public void removeProduct() {
        isActive = !isActive;
        if (isActive) {
            log.info("Item will be deleted");
            remove.getStyleClass().clear();
            remove.getStyleClass().add(remove.getStyleClass().size(), "removeActive");
            setEverythingDisabled(true);
        } else {
            log.info("Item won't be deleted");
            setEverythingDisabled(false);
            remove.getStyleClass().clear();
            remove.getStyleClass().add("button");
        }
    }
}
