package ee.ut.math.tvt.salessystem.ui;

import ee.ut.math.tvt.salessystem.dao.HibernateSalesSystemDAO;
import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.data.HistoryItem;
import ee.ut.math.tvt.salessystem.data.SoldItem;
import ee.ut.math.tvt.salessystem.data.StockItem;
import ee.ut.math.tvt.salessystem.exceptions.SalesSystemException;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A simple CLI (limited functionality).
 */
public class ConsoleUI {
    private static final Logger log = LogManager.getLogger(ConsoleUI.class);
    private final SalesSystemDAO dao;
    private final ShoppingCart cart;
    private boolean running = true;

    private ConsoleUI(SalesSystemDAO dao) {
        this.dao = dao;
        cart = new ShoppingCart(dao);
    }

    public static void main(String[] args) throws Exception {
        log.info("Initializing CLI");
        SalesSystemDAO dao = new HibernateSalesSystemDAO();
        ConsoleUI console = new ConsoleUI(dao);
        console.run();
        dao.close();
    }

    /**
     * Run the sales system CLI.
     */
    private void run() throws IOException {
        print("=============================");
        print("===     Sales  System     ===");
        print("=============================");
        printUsage();
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        while (running) {
            System.out.print("> ");
            processCommand(in.readLine().trim().toLowerCase());
            print("Done.");
        }
    }

    private void printUsage() {
        log.info("Showing help.");
        printLine();
        print("Usage:");
        print("h/help/usage              Show help.");
        print("sw/warehouse              Show warehouse contents.");
        print("sc/cart                   Show shopping cart contents.");
        print("sh/history /arg           Show history. Arg filter: 'today', 'last10' or product ID/name.");
        print("st/team                   Show team information.");
        print("ac/addcart ID/N Q         Add quantity Q of item index ID or \"name\" N to cart.");
        print("ec/editcart ID/N Q        Edit quantity of an item index ID, \"name\" N in cart");
        print("pc/purchase               Confirm shopping cart purchase.");
        print("rc/reset                  Reset shopping cart contents.");
        print("cc/change arg             Calculate change from given amount.");
        print("aw/addwarehouse ID N P Q  Add item index ID, \"name\" N, price P, quantity Q to warehouse.");
        print("ew ID/N -a/-r/-t/-n arg   Edit item index ID or \"name\" N, -a:add/-r:remove/-t:total quantity, -n edit name.");
        print("rm/remove ID/N            Deletes item index ID, \"name\" N from warehouse.");
        print("q/quit/exit               Quit.");
        printLine();
    }

    private void showWarehouse() {
        log.info("Showing warehouse.");
        printLine();
        List<StockItem> items = dao.findStockItems();
        items.forEach(i -> print(i.getId() + " " + i.getName() + " " + i.getPrice() + " € (" +
                i.getQuantity() + " items)"));
        if (items.size() == 0) {
            print("\tThe warehouse is empty.");
        }
        printLine();
    }

    private void showCart() {
        log.info("Showing cart.");
        printLine();
        print("Total cost: " + cart.getTotalCost() + " €");
        cart.getAll().forEach(i -> print(i.getName() + " " + i.getPrice() + " € (" + i.getQuantity() + " items)"));
        if (cart.getAll().size() == 0) {
            print("\tThe cart is empty.");
        }
        printLine();
    }

    private void showTeam() {
        log.info("Showing team.");
        printLine();
        Properties props = new Properties();
        try (InputStream in = new BufferedInputStream(
                getClass().getResourceAsStream("/ee/ut/math/tvt/salessystem/application.properties"))) {
            props.load(in);
            print("Team name: " + props.getProperty("teamName"));
            print("Team leader: " + props.getProperty("teamLeader"));
            print("Team leader email: " + props.getProperty("teamLeaderEmail"));
            print("Team members: " + props.getProperty("teamMember1") + ", " + props.getProperty("teamMember2"));
            try (BufferedReader r = new BufferedReader(new InputStreamReader(
                    getClass().getResourceAsStream("/ee/ut/math/tvt/salessystem/ui/logo.txt"), "UTF-8"))) {
                String line;
                while ((line = r.readLine()) != null) {
                    print(line);
                }
            } catch (IOException e) {
                log.error("Could not load team logo file.", e);
            }
        } catch (IOException e) {
            log.error("Could not load properties file.", e);
        }
        printLine();
    }

    private void addCart(String... c) { //[ac, id/name, quantity]
        log.info("Adding item to cart.");
        if (c.length != 3) {
            print("Invalid amount of arguments.");
            return;
        }
        StockItem item = null;
        try {
            long id = Long.parseLong(c[1]);
            item = dao.findStockItem(id);
        } catch (NumberFormatException e) {
            log.debug(e.getMessage(), e);
        }
        if (item == null) {
            item = dao.findStockItem(c[1]);
        }
        try {
            int amount = Integer.parseInt(c[2]);
            if (item != null) {
                if (cart.hasEnoughTotalStock(item, amount)) {
                    cart.addItem(new SoldItem(item, amount));
                } else {
                    print("Not enough stock available.");
                }
            } else {
                print("No stock item found.");
            }
        } catch (NumberFormatException e) {
            log.debug(e.getMessage(), e);
            print("Invalid quantity.");
        }
    }

    private void editCart(String[] c) {// [ec/editcart, ID/N, Q]
        if (c.length != 3) {
            print("Invalid amount of arguments.");
            return;
        }
        SoldItem soldItem = null;
        try {
            long id = Long.parseLong(c[1]);
            soldItem = cart.getItem(id);
        } catch (NumberFormatException e) {
            log.debug(e.getMessage(), e);
        }
        if (soldItem == null) {
            soldItem = cart.getItem(dao.findStockItem(c[1]).getId());
        }
        try {
            int amount = Integer.parseInt(c[2]);
            if (amount < 0) {
                print("Quantity must not be negative");
                return;
            }
            if (amount == 0) {
                cart.removeItem(soldItem);
            }
            if (soldItem != null) {
                if (amount <= soldItem.getQuantity()) {
                    soldItem.setQuantity(amount);
                } else if (cart.hasEnoughStock(dao.findStockItem(soldItem.getId()), amount)) {
                    soldItem.setQuantity(amount);
                } else {
                    print("Not enough stock available.");
                }
            } else {
                print("No stock item found.");
            }
        } catch (NumberFormatException e) {
            log.debug(e.getMessage(), e);
            print("Invalid quantity.");
        }
    }

    private void addWarehouse(String... c) { // [addwarehouse, ID, N, P, Q]
        if (c.length != 5) {
            print("Invalid amount of arguments.");
            return;
        }
        long id;
        String name = c[2];
        double price;
        int quantity;
        try {
            id = Long.parseLong(c[1]);
            if (!isNameValid(name)) {
                throw new SalesSystemException("Invalid name.");
            }
        } catch (NumberFormatException | SalesSystemException e) {
            log.debug(e.getMessage(), e);
            print("Invalid ID/Name.");
            return;
        }
        try {
            price = Double.parseDouble(c[3]);
            quantity = Integer.parseInt(c[4]);
        } catch (NumberFormatException e) {
            log.debug(e.getMessage(), e);
            print("Invalid price/quantity argument.");
            return;
        }
        StockItem item = new StockItem(id, name, price, quantity);
        dao.beginTransaction();
        if (!dao.replaceStockItem(item)) {
            dao.saveStockItem(item);
        }
        dao.commitTransaction();
    }

    private void showHistory(String[] c) {
        log.info("Showing history.");
        printLine();
        if (c.length == 1) {
            if (dao.findHistoryItems().size() > 0) {
                dao.findHistoryItems().forEach(this::printHistoryItem);
            } else {
                print("\tThe history is empty.");
            }
        } else if (c.length != 2) {
            print("Invalid amount of arguments.");
        } else {
            List<HistoryItem> list = null;
            switch (c[1]) {
                case "today":
                    list = dao.findHistoryItems().stream()
                            .filter(item -> item.getDate().equals(LocalDate.now()))
                            .collect(Collectors.toList());
                    print("Daily income: " + list.stream().mapToDouble(HistoryItem::getTotal).sum() + " €");
                case "last10":
                    list = dao.findHistoryItems().stream()
                            .sorted(Comparator.comparing(HistoryItem::getDate))
                            .limit(10)
                            .collect(Collectors.toList());
            }
            if (list == null) {
                try {
                    long id = Long.parseLong(c[1]);
                    list = dao.findHistoryItems().stream()
                            .filter(item -> item.getItems().stream()
                                    .anyMatch(soldItem -> soldItem.getId().equals(id)))
                            .collect(Collectors.toList());
                } catch (NumberFormatException e) {
                    log.debug(e.getMessage(), e);
                }
                if (list == null || list.isEmpty()) {
                    list = dao.findHistoryItems().stream()
                            .filter(item -> item.getItems().stream()
                                    .anyMatch(soldItem -> soldItem.getName().toLowerCase().equals(c[1].toLowerCase())))
                            .collect(Collectors.toList());
                }
            }
            if (list != null && !list.isEmpty()) {
                list.forEach(this::printHistoryItem);
            } else {
                print("\tNo history items found with filter: " + c[1]);
            }
        }
        printLine();
    }

    private void printHistoryItem(HistoryItem item) {
        print("Sale: " + item.getDate() + ", Total: " + item.getTotal() + " €");
        item.getItems().forEach(soldItem -> print("   " +
                "SoldItem: ID: " + soldItem.getId() +
                ", Name: " + soldItem.getName() +
                ", Price: " + soldItem.getPrice() + " €" +
                ", Quantity: " + soldItem.getQuantity() +
                ", Sum: " + soldItem.getSum() + " €"));
    }

    private void showChange(String[] c) {
        log.info("Showing change.");
        printLine();
        if (c.length == 1) {
            print("Please specify given amount.");
            printLine();
            return;
        }
        try {
            double amount = Double.parseDouble(c[1]);
            print("Change: " + cart.calculateChange(amount, cart.getTotalCost()));
        } catch (NumberFormatException e) {
            log.debug(e.getMessage(), e);
            print("Invalid argument.");
        }
        printLine();
    }

    private void purchaseCart() {
        if (cart.getAll().isEmpty()) {
            print("\tThe cart is empty.");
            return;
        }
        cart.submitCurrentPurchase();
    }

    private void editWarehouseQuantity(String[] c) {//[ewq, ID/N, -a/-r/-t/-n, arg]
        if (c.length != 4) {
            print("Invalid amount of arguments.");
            return;
        }
        dao.beginTransaction();
        try {
            StockItem item = null;
            try {
                long id = Long.parseLong(c[1]);
                item = dao.findStockItem(id);
            } catch (NumberFormatException e) {
                log.debug(e.getMessage(), e);
            }
            if (item == null) {
                item = dao.findStockItem(c[1]);
            }
            if (item == null) {
                print("No stock item found.");
                return;
            }
            try {
                if (!c[2].equals("-n")) {
                    int amount = Integer.parseInt(c[3]);
                    if (amount < 0) {
                        print("Quantity must not be negative");
                        return;
                    }
                    switch (c[2]) {
                        case "-a":
                            item.setQuantity(item.getQuantity() + amount);
                            break;
                        case "-r":
                            if (item.getQuantity() < amount) {
                                print("Cannot remove " + amount + " items if quantity is " + item.getQuantity() + ".");
                                return;
                            }
                            item.setQuantity(item.getQuantity() - amount);
                            break;
                        case "-t":
                            item.setQuantity(amount);
                            break;
                        default:
                            print("Invalid attribute");
                    }
                } else if (c[2].equals("-n")) {
                    item.setName(c[3]);
                } else {
                    print("Invalid attribute");
                }
            } catch (NumberFormatException e) {
                log.debug(e.getMessage(), e);
                print("Invalid quantity.");
            }
        } finally {
            dao.commitTransaction();
        }

    }

    private void deleteItem(String[] c) { //[rm/remove, ID/N]
        if (c.length != 2) {
            print("Invalid amount of arguments.");
            return;
        }
        dao.beginTransaction();
        try {
            StockItem item = null;
            try {
                long id = Long.parseLong(c[1]);
                item = dao.findStockItem(id);
            } catch (NumberFormatException e) {
                log.debug(e.getMessage(), e);
            }
            if (item == null) {
                item = dao.findStockItem(c[1]);
            }
            if (item == null) {
                print("No stock item found.");
                return;
            }
            while (true) {
                print("Do you really want to remove item: " + item.getName() + "/" + item.getId() + "? [y/n]");
                BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
                try {
                    String input = in.readLine().trim().toLowerCase();
                    System.out.println(input);
                    switch (input) {
                        case "y":
                            dao.removeStockItem(item);
                            return;
                        case "n":
                            return;
                        default:
                            print("Unknown command");
                    }
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
            }
        } finally {
            dao.commitTransaction();
        }
    }

    private String[] getCommand(String command) { // rm "bubble gum"
        List<String> matchList = new ArrayList<>();
        if (!command.contains("\"")) {
            return command.split(" ");
        } else {
            Pattern regex = Pattern.compile("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'");
            Matcher regexMatcher = regex.matcher(command);
            while (regexMatcher.find()) {
                if (regexMatcher.group(1) != null) {
                    matchList.add(regexMatcher.group(1));
                } else if (regexMatcher.group(2) != null) {
                    matchList.add(regexMatcher.group(2));
                } else {
                    matchList.add(regexMatcher.group());
                }
            }
            return matchList.toArray(new String[matchList.size()]);
        }
    }

    private void processCommand(String command) {
        String[] splitted = getCommand(command);
        switch (splitted[0]) {
            case "h":
            case "help":
            case "usage":
                printUsage();
                break;
            case "q":
            case "quit":
            case "exit":
                running = false;
                break;
            case "st":
            case "team":
                showTeam();
                break;
            case "sw":
            case "warehouse":
                showWarehouse();
                break;
            case "sh":
            case "history":
                showHistory(splitted);
                break;
            case "sc":
            case "cart":
                showCart();
                break;
            case "pc":
            case "purchase":
                purchaseCart();
                break;
            case "rc":
            case "reset":
                cart.cancelCurrentPurchase();
                break;
            case "cc":
            case "change":
                showChange(splitted);
                break;
            case "ac":
            case "addcart":
                addCart(splitted);
                break;
            case "aw":
            case "addwarehouse":
                addWarehouse(splitted);
                break;
            case "ew":
                editWarehouseQuantity(splitted);
                break;
            case "rm":
            case "remove":
                deleteItem(splitted);
                break;
            case "ec":
            case "editcart":
                editCart(splitted);
                break;
            default:
                print("Unknown command.");
        }
    }

    private boolean isNameValid(String name) {
        return !StringUtils.isNumeric(name) && name.length() > 0;
    }

    private void printLine() {
        print("-----------------------------");
    }

    private void print(String str) {
        System.out.println(str);
    }

}