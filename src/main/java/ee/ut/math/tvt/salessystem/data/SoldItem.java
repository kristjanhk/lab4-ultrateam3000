package ee.ut.math.tvt.salessystem.data;

import javax.persistence.*;

/**
 * Already bought StockItem. SoldItem duplicates given StockItem for preserving history.
 */
@Entity
@Table(name = "Sold")
public class SoldItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SuppressWarnings("unused")
    private Long soldId;

    @ManyToOne
    @SuppressWarnings("all")
    private HistoryItem parent;

    private Long id;
    private String name;
    private int quantity;
    private double price;

    public SoldItem(StockItem stockItem, int quantity) {
        this.id = stockItem.getId();
        this.name = stockItem.getName();
        this.quantity = quantity;
        this.price = stockItem.getPrice();
    }

    protected SoldItem() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setParent(HistoryItem parent) {
        this.parent = parent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double getSum() {
        return price * quantity;
    }

    @Override
    public String toString() {
        return id + "\t" + name + "\t" + price + "\t" + quantity + "\n";
    }
}
