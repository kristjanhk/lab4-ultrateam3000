package ee.ut.math.tvt.salessystem.exceptions.database;

public class DatabaseCorruptException extends DatabaseException {
    public DatabaseCorruptException() {
    }

    public DatabaseCorruptException(String message) {
        super(message);
    }

    public DatabaseCorruptException(String message, Throwable cause) {
        super(message, cause);
    }
}
