package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.data.HistoryItem;
import ee.ut.math.tvt.salessystem.data.SoldItem;
import ee.ut.math.tvt.salessystem.data.StockItem;
import ee.ut.math.tvt.salessystem.exceptions.database.DatabaseCorruptException;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class InMemorySalesSystemDAO implements SalesSystemDAO {
    private final List<StockItem> stockItemList = new ArrayList<>();
    private final List<SoldItem> soldItemList = new ArrayList<>();
    private final List<HistoryItem> historyItemList = new ArrayList<>();
    private int beginTransactionCounter = 0;
    private int commitTransactionCounter = 0;
    private boolean isCorrectOrder = true;

    public InMemorySalesSystemDAO() {
        stockItemList.add(new StockItem(1L, "Lays chips", 11.00, 10));
        stockItemList.add(new StockItem(2L, "Chupa-chups", 8.00, 15));
        stockItemList.add(new StockItem(3L, "Frankfurters", 15.00, 20));
        stockItemList.add(new StockItem(4L, "Bubble gum", 3.00, 25));
        stockItemList.add(new StockItem(5L, "Milk", 2.00, 80));
        stockItemList.add(new StockItem(6L, "Meat", 20.00, 30));
        stockItemList.add(new StockItem(7L, "Chocolate", 4.50, 60));
        stockItemList.add(new StockItem(8L, "Bread", 1.50, 150));
        stockItemList.add(new StockItem(9L, "Free Beer", 0.00, 50));
        stockItemList.add(new StockItem(10L, "Coffee", 10.00, 35));
        stockItemList.add(new StockItem(11L, "Tea", 4.00, 70));
        stockItemList.add(new StockItem(12L, "Water", 0.50, 200));
    }

    @Override
    public List<StockItem> findStockItems() {
        return stockItemList;
    }

    @Override
    public StockItem findStockItem(long id) {
        return stockItemList.stream()
                .filter(item -> item.getId() == id)
                .findFirst()
                .orElse(null);
    }

    @Override
    public StockItem findStockItem(String name) {
        return stockItemList.stream()
                .filter(item -> item.getName().toLowerCase().equals(name.toLowerCase()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        soldItemList.add(item);
    }

    @Override
    public List<HistoryItem> findHistoryItems() {
        return historyItemList;
    }

    @Override
    public void saveHistoryItem(HistoryItem item) {
        historyItemList.add(item);
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        stockItemList.add(stockItem);
    }

    @Override
    public boolean replaceStockItem(StockItem stockItem) {
        StockItem old = findStockItem(stockItem.getId());
        if (old != null) {
            stockItemList.set(stockItemList.indexOf(old), stockItem);
            return true;
        }
        return false;
    }

    @Override
    public void removeStockItem(StockItem item, int amount) {
        item.setQuantity(item.getQuantity() - amount);
    }

    @Override
    public void removeStockItem(StockItem item) {
        stockItemList.remove(item);
    }

    @Override
    public void beginTransaction() {
        beginTransactionCounter++;
    }


    @Override
    public void rollbackTransaction() {

    }

    @Override
    public void commitTransaction() {
        if (beginTransactionCounter <= commitTransactionCounter) {
            isCorrectOrder = false;
        }
        commitTransactionCounter++;
    }

    public int areBeginAndCommitTransactionsCalled() {
        if (beginTransactionCounter != commitTransactionCounter && isCorrectOrder) {
            throw new DatabaseCorruptException("Begin and commit transaction counters do not match!");
        }
        return beginTransactionCounter;
    }

    @Override
    public void close() {

    }

    @Override
    public List<HistoryItem> findHistoryInBetween(LocalDate startData, LocalDate endDate, String filter) {
        return null;
    }

    @Override
    public List<HistoryItem> findHistoryItems(String filter) {
        return null;
    }

    @Override
    public List<HistoryItem> find10LastHistoryItems(String filter) {
        return null;
    }
}
