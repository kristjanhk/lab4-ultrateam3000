package ee.ut.math.tvt.salessystem.exceptions.date;

public class InvalidDateFormatException extends InvalidDateException {
    public InvalidDateFormatException() {
    }

    public InvalidDateFormatException(String message) {
        super(message);
    }

    public InvalidDateFormatException(String message, Throwable cause) {
        super(message, cause);
    }
}
