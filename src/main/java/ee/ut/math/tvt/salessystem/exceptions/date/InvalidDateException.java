package ee.ut.math.tvt.salessystem.exceptions.date;

import ee.ut.math.tvt.salessystem.exceptions.input.InvalidInputException;

public class InvalidDateException extends InvalidInputException {
    public InvalidDateException() {
    }

    public InvalidDateException(String message) {
        super(message);
    }

    public InvalidDateException(String message, Throwable cause) {
        super(message, cause);
    }
}
