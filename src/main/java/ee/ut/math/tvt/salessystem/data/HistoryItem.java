package ee.ut.math.tvt.salessystem.data;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "History")
public class HistoryItem implements Comparable<HistoryItem> {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @SuppressWarnings("unused")
    private Long id;

    private LocalDateTime date;

    @OneToMany(mappedBy = "parent")
    private List<SoldItem> items;

    public HistoryItem(LocalDateTime date) {
        this.date = date;
        this.items = new ArrayList<>();
    }

    protected HistoryItem() {
    }

    public LocalDate getDate() {
        return date.toLocalDate();
    }

    public LocalTime getTime() {
        return date.toLocalTime();
    }

    public List<SoldItem> getItems() {
        return items;
    }

    public void addItem(SoldItem item) {
        items.add(item);
    }

    public double getTotal() {
        return this.items.stream().mapToDouble(item -> item.getPrice() * item.getQuantity()).sum();
    }

    public void removeItem(SoldItem soldItem) {
        if (soldItem != null) {
            items.remove(soldItem);
            soldItem.setParent(null);
        }
    }

    @Override
    public int compareTo(HistoryItem o) {
        return date.toLocalDate().compareTo(o.getDate());
    }

    @Override
    public String toString() {
        return date + "\t" + getTotal();
    }
}
