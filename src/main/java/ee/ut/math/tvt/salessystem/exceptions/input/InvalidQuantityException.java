package ee.ut.math.tvt.salessystem.exceptions.input;

public class InvalidQuantityException extends InvalidInputException {
    public InvalidQuantityException() {
    }

    public InvalidQuantityException(String message) {
        super(message);
    }

    public InvalidQuantityException(String message, Throwable cause) {
        super(message, cause);
    }
}
