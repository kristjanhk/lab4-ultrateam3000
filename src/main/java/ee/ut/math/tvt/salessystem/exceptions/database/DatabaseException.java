package ee.ut.math.tvt.salessystem.exceptions.database;

import ee.ut.math.tvt.salessystem.exceptions.SalesSystemException;

public class DatabaseException extends SalesSystemException {

    public DatabaseException() {
    }

    public DatabaseException(String message) {
        super(message);
    }

    public DatabaseException(String message, Throwable cause) {
        super(message, cause);
    }
}
