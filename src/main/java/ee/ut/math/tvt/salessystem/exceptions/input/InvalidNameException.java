package ee.ut.math.tvt.salessystem.exceptions.input;

public class InvalidNameException extends InvalidInputException {

    public InvalidNameException() {
    }

    public InvalidNameException(String message) {
        super(message);
    }

    public InvalidNameException(String message, Throwable cause) {
        super(message, cause);
    }
}
