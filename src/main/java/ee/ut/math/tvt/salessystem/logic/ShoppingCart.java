package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.SalesSystemDAO;
import ee.ut.math.tvt.salessystem.data.HistoryItem;
import ee.ut.math.tvt.salessystem.data.SoldItem;
import ee.ut.math.tvt.salessystem.data.StockItem;
import ee.ut.math.tvt.salessystem.exceptions.cart.InvalidCartException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class ShoppingCart {
    private static final Logger log = LogManager.getLogger(ShoppingCart.class);
    private final SalesSystemDAO dao;
    private final List<SoldItem> items = new ArrayList<>();

    public ShoppingCart(SalesSystemDAO dao) {
        this.dao = dao;
    }

    /**
     * Add new SoldItem to table.
     */
    public void addItem(SoldItem item) {
        if (item.getQuantity() <= 0) {
            throw new InvalidCartException("Added item's quantity is negative!");
        }
        SoldItem prevItem = getItem(item.getId());
        if (prevItem != null) {
            if (!hasEnoughTotalStock(dao.findStockItem(item.getId()), item.getQuantity())) {
                throw new InvalidCartException(
                        "Added item's quantity and quantity in cart is larger than available stock!");
            }
            prevItem.setQuantity(prevItem.getQuantity() + item.getQuantity());
        } else {
            if (!hasEnoughStock(dao.findStockItem(item.getId()), item.getQuantity())) {
                throw new InvalidCartException("Added item's quantity is larger than available stock!");
            }
            items.add(item);
        }
        log.debug("Added " + item.getName() + " quantity of " + item.getQuantity());
    }

    public void removeItem(SoldItem item) {
        items.remove(item);
    }

    public List<SoldItem> getAll() {
        return items;
    }

    public SoldItem getItem(Long id) {
        return items.stream()
                .filter(i -> i.getId().equals(id))
                .findAny()
                .orElse(null);
    }

    public int getItemQuantityIfExists(StockItem item) {
        SoldItem soldItem = getItem(item.getId());
        return soldItem != null ? soldItem.getQuantity() : 0;
    }

    public void cancelCurrentPurchase() {
        items.clear();
    }

    public void submitCurrentPurchase() {
        dao.beginTransaction();
        try {
            HistoryItem historyItem = new HistoryItem(LocalDateTime.now());
            dao.saveHistoryItem(historyItem);
            items.forEach(item -> {
                item.setParent(historyItem);
                dao.saveSoldItem(item);
                historyItem.addItem(item);
                dao.removeStockItem(dao.findStockItem(item.getId()), item.getQuantity());
            });
            System.out.println(items);
            dao.commitTransaction();
            items.clear();
        } catch (Exception e) {
            dao.rollbackTransaction();
            throw e;
        }
    }

    /**
     * Calculates difference between two given amounts.
     */
    public double calculateChange(double total, double given) {
        return total - given;
    }

    /**
     * Checks if warehouse has enough stock based on given and cart quantities.
     */
    public boolean hasEnoughTotalStock(StockItem item, int amount) {
        if (item == null) {
            return false;
        }
        SoldItem soldItem = getItem(item.getId());
        return (soldItem != null ? soldItem.getQuantity() : 0) + amount <= item.getQuantity();
    }

    /**
     * Checks if warehouse has enough stock based on given quantity.
     */
    public boolean hasEnoughStock(StockItem item, int amount) {
        return amount <= item.getQuantity();
    }

    /**
     * Calculates the total cost of purchase
     */
    public double getTotalCost() {
        return items.stream().mapToDouble(SoldItem::getSum).sum();
    }
}