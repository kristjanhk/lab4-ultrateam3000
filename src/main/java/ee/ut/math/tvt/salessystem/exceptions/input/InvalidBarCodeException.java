package ee.ut.math.tvt.salessystem.exceptions.input;

public class InvalidBarCodeException extends InvalidInputException {

    public InvalidBarCodeException() {
    }

    public InvalidBarCodeException(String message) {
        super(message);
    }

    public InvalidBarCodeException(String message, Throwable cause) {
        super(message, cause);
    }
}
