package ee.ut.math.tvt.salessystem.dao;

import ee.ut.math.tvt.salessystem.data.HistoryItem;
import ee.ut.math.tvt.salessystem.data.SoldItem;
import ee.ut.math.tvt.salessystem.data.StockItem;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.time.LocalDate;
import java.util.List;

public class HibernateSalesSystemDAO implements SalesSystemDAO {
    private final EntityManagerFactory emf;
    private final EntityManager em;

    public HibernateSalesSystemDAO() {
        emf = Persistence.createEntityManagerFactory("pos");
        em = emf.createEntityManager();
    }

    @Override
    public List<StockItem> findStockItems() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<StockItem> cq = cb.createQuery(StockItem.class);
        Root<StockItem> item = cq.from(StockItem.class);
        cq.select(item.as(StockItem.class));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public StockItem findStockItem(long id) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<StockItem> cq = cb.createQuery(StockItem.class);
        Root<StockItem> item = cq.from(StockItem.class);
        cq.select(item.as(StockItem.class));
        cq.where(cb.equal(item.get("id"), id));
        TypedQuery<StockItem> q = em.createQuery(cq).setFirstResult(0).setMaxResults(1);
        try {
            return q.getSingleResult();
        } catch (Exception ignored) {
            return null;
        }
    }

    @Override
    public StockItem findStockItem(String name) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<StockItem> cq = cb.createQuery(StockItem.class);
        Root<StockItem> item = cq.from(StockItem.class);
        cq.select(item.as(StockItem.class));
        cq.where(cb.equal(item.get("name"), name));
        TypedQuery<StockItem> q = em.createQuery(cq).setFirstResult(0).setMaxResults(1);
        try {
            return q.getSingleResult();
        } catch (NullPointerException | NoResultException e) {
            return null;
        }
    }

    @Override
    public void saveStockItem(StockItem stockItem) {
        em.persist(stockItem);
        em.flush();
    }

    @Override
    public boolean replaceStockItem(StockItem stockItem) {
        boolean replaced = false;
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<StockItem> cq = cb.createQuery(StockItem.class);
        Root<StockItem> item = cq.from(StockItem.class);
        cq.select(item.as(StockItem.class));
        List<StockItem> stock = em.createQuery(cq).getResultList();
        for (StockItem stockItem2 : stock) {
            if (stockItem2.getName().equals(stockItem.getName())) {
                em.remove(stockItem2);
                em.persist(stockItem);
                replaced = true;
            }
        }
        return replaced;
    }

    @Override
    public void removeStockItem(StockItem item, int amount) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<StockItem> cq = cb.createQuery(StockItem.class);
        Root<StockItem> stock = cq.from(StockItem.class);
        cq.select(stock.as(StockItem.class));
        em.createQuery(cq).getResultList().stream()
                .filter(current -> current.getName().equals(item.getName()))
                .forEach(current -> {
                    if (item.getQuantity() - amount > 0) {
                        item.setQuantity(item.getQuantity() - amount);
                    } else {
                        em.remove(current);
                    }
                });
    }

    @Override
    public void removeStockItem(StockItem item) {
        em.remove(item);
    }

    @Override
    public void saveSoldItem(SoldItem item) {
        em.persist(item);
    }

    @Override
    public List<HistoryItem> findHistoryItems() {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<HistoryItem> cq = cb.createQuery(HistoryItem.class);
        Root<HistoryItem> history = cq.from(HistoryItem.class);
        cq.select(history.as(HistoryItem.class));
        return em.createQuery(cq).getResultList();
    }

    @Override
    public List<HistoryItem> findHistoryInBetween(LocalDate startData, LocalDate endDate, String filter) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<HistoryItem> cq = cb.createQuery(HistoryItem.class);
        Root<HistoryItem> history = cq.from(HistoryItem.class);
        Root<SoldItem> sold = cq.from(SoldItem.class);
        cq.select(history.as(HistoryItem.class)).where(cb.and(
                cb.equal(sold.get("parent"), history.get("id")), cb.and(
                        cb.between(history.get("date"), startData.atStartOfDay().minusDays(1),
                                endDate.atStartOfDay().plusDays(1)),
                        StringUtils.isNumeric(filter) ?
                                cb.equal(sold.get("id"), Long.parseLong(filter)) :
                                cb.like(cb.lower(sold.get("name")), "%" + filter + "%"))));
        return em.createQuery(cq.distinct(true)).getResultList();
    }

    @Override
    public List<HistoryItem> findHistoryItems(String filter) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<HistoryItem> cq = cb.createQuery(HistoryItem.class);
        Root<SoldItem> sold = cq.from(SoldItem.class);
        Root<HistoryItem> history = cq.from(HistoryItem.class);
        cq.select(history.as(HistoryItem.class)).where(cb.and(
                cb.equal(sold.get("parent"), history.get("id")),
                StringUtils.isNumeric(filter) ?
                        cb.equal(sold.get("id"), Long.parseLong(filter)) :
                        cb.like(cb.lower(sold.get("name")), "%" + filter + "%")));
        return em.createQuery(cq.distinct(true)).getResultList();
    }

    @Override
    public List<HistoryItem> find10LastHistoryItems(String filter) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<HistoryItem> cq = cb.createQuery(HistoryItem.class);
        Root<SoldItem> sold = cq.from(SoldItem.class);
        Root<HistoryItem> history = cq.from(HistoryItem.class);
        cq.select(history.as(HistoryItem.class)).where(cb.and(
                cb.equal(sold.get("parent"), history.get("id")),
                StringUtils.isNumeric(filter) ?
                        cb.equal(sold.get("id"), Long.parseLong(filter)) :
                        cb.like(cb.lower(sold.get("name")), "%" + filter + "%")));
        cq.orderBy(cb.desc(history.get("date")));
        return em.createQuery(cq.distinct(true)).setMaxResults(10).getResultList();
    }

    @Override
    public void saveHistoryItem(HistoryItem item) {
        em.persist(item);
    }

    @Override
    public void beginTransaction() {
        try {
            em.getTransaction().begin();
        } catch (IllegalStateException ignored) {
            rollbackTransaction();
            beginTransaction();
        }
    }

    @Override
    public void rollbackTransaction() {
        em.getTransaction().rollback();
    }

    @Override
    public void commitTransaction() {
        em.getTransaction().commit();
    }

    @Override
    public void close() {
        em.close();
        emf.close();
    }
}