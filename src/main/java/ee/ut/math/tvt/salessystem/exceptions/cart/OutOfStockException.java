package ee.ut.math.tvt.salessystem.exceptions.cart;

public class OutOfStockException extends InvalidCartException {

    public OutOfStockException() {
    }

    public OutOfStockException(String message) {
        super(message);
    }

    public OutOfStockException(String message, Throwable cause) {
        super(message, cause);
    }
}
