package ee.ut.math.tvt.salessystem.exceptions.cart;

import ee.ut.math.tvt.salessystem.exceptions.SalesSystemException;

public class InvalidCartException extends SalesSystemException {

    public InvalidCartException() {
    }

    public InvalidCartException(String message) {
        super(message);
    }

    public InvalidCartException(String message, Throwable cause) {
        super(message, cause);
    }
}
