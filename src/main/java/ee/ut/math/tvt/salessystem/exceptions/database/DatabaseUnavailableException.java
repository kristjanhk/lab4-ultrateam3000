package ee.ut.math.tvt.salessystem.exceptions.database;

public class DatabaseUnavailableException extends DatabaseException {
    public DatabaseUnavailableException() {
    }

    public DatabaseUnavailableException(String message) {
        super(message);
    }

    public DatabaseUnavailableException(String message, Throwable cause) {
        super(message, cause);
    }
}
