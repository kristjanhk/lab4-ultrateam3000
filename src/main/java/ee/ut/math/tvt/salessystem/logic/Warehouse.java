package ee.ut.math.tvt.salessystem.logic;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.data.HistoryItem;
import ee.ut.math.tvt.salessystem.data.StockItem;
import ee.ut.math.tvt.salessystem.exceptions.database.DatabaseException;

import java.util.List;

public class Warehouse {
    private final InMemorySalesSystemDAO dao;

    public Warehouse(InMemorySalesSystemDAO dao) {
        this.dao = dao;
    }

    public List<StockItem> findStockItems() {
        return dao.findStockItems();
    }

    public StockItem findStockItem(long id) {
        return dao.findStockItem(id);
    }

    public void saveStockItem(StockItem item) {
        if (item.getQuantity() <= 0) {
            throw new DatabaseException("Saved item's quantity is negative!");
        }
        StockItem savedItem = findStockItem(item.getId());
        dao.beginTransaction();
        if (savedItem == null) {
            dao.saveStockItem(item);
        } else {
            savedItem.setQuantity(savedItem.getQuantity() + item.getQuantity());
        }
        dao.commitTransaction();
    }

    public int areBeginAndCommitTransactionsCalled() {
        return dao.areBeginAndCommitTransactionsCalled();
    }

    public List<HistoryItem> findHistoryItems() {
        return dao.findHistoryItems();
    }
}
