package ee.ut.math.tvt.salessystem.exceptions.input;

import ee.ut.math.tvt.salessystem.exceptions.SalesSystemException;

public class InvalidInputException extends SalesSystemException {

    public InvalidInputException() {
    }

    public InvalidInputException(String message) {
        super(message);
    }

    public InvalidInputException(String message, Throwable cause) {
        super(message, cause);
    }
}
