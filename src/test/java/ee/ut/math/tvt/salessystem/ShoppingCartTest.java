package ee.ut.math.tvt.salessystem;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.data.SoldItem;
import ee.ut.math.tvt.salessystem.data.StockItem;
import ee.ut.math.tvt.salessystem.exceptions.cart.InvalidCartException;
import ee.ut.math.tvt.salessystem.logic.ShoppingCart;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalTime;

import static org.junit.Assert.*;

public class ShoppingCartTest {
    private Warehouse warehouse;
    private ShoppingCart cart;
    private StockItem stockItem = new StockItem(15L, "Lauaviin", 7.0, 15);
    private SoldItem soldItem;

    @Before
    public void setup() {
        InMemorySalesSystemDAO dao = new InMemorySalesSystemDAO();
        warehouse = new Warehouse(dao);
        cart = new ShoppingCart(dao);
        soldItem = new SoldItem(stockItem, 5);
        warehouse.saveStockItem(stockItem);
    }

    @Test
    public void testAddingExistingItem() {
        int startQuantity = 5;
        int addedQuantity = 3;
        soldItem.setQuantity(startQuantity);
        cart.addItem(soldItem);
        SoldItem newItem = new SoldItem(stockItem, addedQuantity);
        cart.addItem(newItem);
        assertEquals("Cart contains more items than allowed!", 1,
                cart.getAll().stream()
                        .filter(i -> i.getId().equals(soldItem.getId()))
                        .count());
        assertEquals("Added soldItem quantity is not correct!", startQuantity + addedQuantity,
                cart.getItem(soldItem.getId()).getQuantity());
    }

    @Test
    public void testAddingNewItem() {
        cart.addItem(soldItem);
        assertNotNull("Cart did not save soldItem!", cart.getItem(soldItem.getId()));
    }

    @Test(expected = InvalidCartException.class)
    public void testAddingItemWithNegativeQuantity() {
        soldItem.setQuantity(-5);
        cart.addItem(soldItem);
    }

    @Test(expected = InvalidCartException.class)
    public void testAddingItemWithQuantityTooLarge() {
        soldItem.setQuantity(stockItem.getQuantity() + 500);
        cart.addItem(soldItem);
    }

    @Test(expected = InvalidCartException.class)
    public void testAddingItemWithQuantitySumTooLarge() {
        cart.addItem(soldItem);
        SoldItem newItem = new SoldItem(stockItem, stockItem.getQuantity() - 2);
        cart.addItem(newItem);
    }

    @Test
    public void testSubmittingCurrentPurchaseDecreasesStockItemQuantity() {
        int initialQuantity = stockItem.getQuantity();
        cart.addItem(soldItem);
        cart.submitCurrentPurchase();
        assertEquals("Submitting purchase did not decrease stock quantity!",
                initialQuantity - soldItem.getQuantity(),
                warehouse.findStockItem(soldItem.getId()).getQuantity());
    }

    @Test
    public void testSubmittingCurrentPurchaseBeginsAndCommitsTransaction() {
        cart.addItem(soldItem);
        cart.submitCurrentPurchase();
        //1 set of transactions are done when saving stockitem so we expect 2
        assertEquals("DAO begin and commit transactions were not called properly!", 2,
                warehouse.areBeginAndCommitTransactionsCalled());
    }

    @Test
    public void testSubmittingCurrentOrderCreatesHistoryItem() {
        int historyItemsCount = warehouse.findHistoryItems().size();
        cart.addItem(soldItem);
        cart.submitCurrentPurchase();
        assertEquals("Submitting purchase did not add new HistoryItem!", historyItemsCount + 1,
                warehouse.findHistoryItems().size());
        assertTrue("Saved HistoryItem did not contain purchased soldItem!", warehouse.findHistoryItems()
                .get(warehouse.findHistoryItems().size() - 1).getItems().contains(soldItem));
    }

    @Test
    public void testSubmittingCurrentOrderSavesCorrectTime() {
        cart.addItem(soldItem);
        cart.submitCurrentPurchase();
        assertEquals(LocalTime.now().toSecondOfDay(), warehouse.findHistoryItems()
                .get(warehouse.findHistoryItems().size() - 1).getTime().toSecondOfDay(), 1.0);

    }

    @Test
    public void testCancellingOrder() {
        StockItem newStockItem = new StockItem(20L, "Lauaviin väikese kurgiga", 8.0, 10);
        SoldItem newSoldItem = new SoldItem(newStockItem, 5);
        warehouse.saveStockItem(newStockItem);
        cart.addItem(soldItem);
        cart.cancelCurrentPurchase();
        cart.addItem(newSoldItem);
        assertFalse("Cancelling purchase did not clear cart!", cart.getAll().contains(soldItem));
    }

    @Test
    public void testCancellingOrderQuantitiesUnchanged() {
        int quantity = stockItem.getQuantity();
        cart.addItem(soldItem);
        cart.cancelCurrentPurchase();
        assertEquals("Cancelling purchase changed stock quantity!", quantity,
                warehouse.findStockItem(soldItem.getId()).getQuantity());
    }
}