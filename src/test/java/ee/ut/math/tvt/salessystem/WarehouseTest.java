package ee.ut.math.tvt.salessystem;

import ee.ut.math.tvt.salessystem.dao.InMemorySalesSystemDAO;
import ee.ut.math.tvt.salessystem.data.StockItem;
import ee.ut.math.tvt.salessystem.exceptions.database.DatabaseException;
import ee.ut.math.tvt.salessystem.logic.Warehouse;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class WarehouseTest {
    private Warehouse warehouse;
    private StockItem item;

    @Before
    public void setup() {
        warehouse = new Warehouse(new InMemorySalesSystemDAO());
        item = new StockItem(15L, "Lauaviin", 7.0, 15);
    }

    @Test
    public void testAddingItemBeginsAndCommitsTransaction() {
        warehouse.saveStockItem(item);
        assertEquals("DAO begin and commit transactions were not called properly!", 1,
                warehouse.areBeginAndCommitTransactionsCalled());
    }

    @Test
    public void testAddingNewItem() {
        warehouse.saveStockItem(item);
        assertNotNull("StockItem was not saved to warehouse!", warehouse.findStockItem(item.getId()));
    }

    @Test
    public void testAddingExistingItem() {
        int startQuantity = 15;
        int addingQuantity = 25;
        item.setQuantity(startQuantity);
        warehouse.saveStockItem(item);
        StockItem newItem = new StockItem(item.getId(), item.getName(), item.getPrice(), addingQuantity);
        warehouse.saveStockItem(newItem);
        assertEquals("DAO contains more items than allowed!", 1,
                warehouse.findStockItems().stream()
                        .filter(i -> i.getId().equals(item.getId()))
                        .count());
        assertEquals("Added item quantity is not correct!", startQuantity + addingQuantity,
                warehouse.findStockItem(item.getId()).getQuantity());
    }

    @Test(expected = DatabaseException.class)
    public void testAddingItemWithNegativeQuantity() {
        item.setQuantity(-5);
        warehouse.saveStockItem(item);
    }
}
