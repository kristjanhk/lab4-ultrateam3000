##Welcome

###Team members
* Kristjan Hendrik Küngas
* Kristen Kotkas
* Heiti Ehrpais

###Homework
[Lab 1 - Requirements Gathering](https://bitbucket.org/kristjanhk/lab4-ultrateam3000/wiki/Homework%201)  
[Lab 2 - Requirements Specification, Modeling, Planning](https://bitbucket.org/kristjanhk/lab4-ultrateam3000/wiki/Homework%202)  
[Lab 3 - Development Environment](https://bitbucket.org/kristjanhk/lab4-ultrateam3000/wiki/Homework%203)  
[Lab 4 - Development, Phase I](https://bitbucket.org/kristjanhk/lab4-ultrateam3000/wiki/Homework%204)  
[Lab 5 - Development, Phase II](https://bitbucket.org/kristjanhk/lab4-ultrateam3000/wiki/Homework%205)  
[Lab 6 - Automatic Tests & Refactoring](https://bitbucket.org/kristjanhk/lab4-ultrateam3000/wiki/Homework%206)  
[Lab 7 - Software Validation & Verification Plan](https://bitbucket.org/kristjanhk/lab4-ultrateam3000/wiki/Homework%207)  